<?php 
session_start();

// Set all boxes to close (1)
$_SESSION['b1'] = 1;
$_SESSION['b2'] = 1;
$_SESSION['b3'] = 1;
$_SESSION['b4'] = 1;
$_SESSION['b5'] = 1;
$_SESSION['b6'] = 1;
$_SESSION['b7'] = 1;
$_SESSION['b8'] = 1;
$_SESSION['b9'] = 1;
$_SESSION['b10'] = 1;
$_SESSION['b11'] = 1;
$_SESSION['b12'] = 1;
$_SESSION['b13'] = 1;
$_SESSION['b14'] = 1;
$_SESSION['b15'] = 1;
$_SESSION['b16'] = 1;
// Set Banker call to 0
$_SESSION['banker'] = 0;
// Set the numbe of boxes operd to 0
$_SESSION['count'] = 0;
// Set offer 
$_SESSION['offer'] = 0;
$_SESSION['lastoffer'];
// Set value if deal is done will remain 0 untill a deal is made.
$_SESSION['deal'] = 0;



?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Deal Or No Deal</title>
<link rel="stylesheet" type="text/css" href="css/newgame.css">
<meta http-equiv="refresh" content="15; url=gameboard.php">

</head>

<body>
<video id="video_background" preload="auto" autoplay="true"  muted volume="0">
<source src="media/LargeDeal.webm" type="video/webm" />
<source src="media/LargeDeal.mp4" type="video/mp4" />          
</video>
</video>
</body>
</html>